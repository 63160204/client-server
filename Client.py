import socket

# Create a socket object
client_socket = socket.socket()

# Define the IP address and port to connect
ip = "192.168.1.105"
port = 12345

# Connect to the server
client_socket.connect((ip, port))

# Read from the socket and write into buffer until EOF
buffer = ""
while True:
    data_received = client_socket.recv(1024).decode()
    if not data_received:
        break
    buffer += data_received

# Print the contents from the buffer
print(buffer)

# Close the connection
client_socket.close()
